const express = require('express');
const fs = require('fs');
const http = require('http');
const path = require('path');

const app = express();

const port = process.env.PORT || 8080

app.use(express.static(__dirname + '/app'));

app.get('/*', (req, res) => res.sendFile(path.join(__dirname)));

const server = http.createServer(app);

server.listen(port,() => console.log('Funcionando'))