var dia_visita = hoje()

function hoje(){
    var data_hoje = new Date();
    var dia = data_hoje.getDate();
    var mes = data_hoje.getMonth()+1;
    var ano = data_hoje.getFullYear();

    if(dia<10)dia='0'+dia
    if(mes<10)mes='0'+mes
    data_hoje = ano+'/'+mes+'/'+dia;

    console.log("HOJE:", data_hoje)
}

function inicio(){
    window.location.assign ("index.html");
}

function baixar(){
    window.open ("landing.html"), '_blank';
}

function baixarChecklist(){
    window.open ("https://drive.google.com/file/d/1J3NA2TstHqnTkFmjOr1IUai6P7llLvu7/view?usp=drivesdk"), '_blank';
}

function baixarSEOList(){
    window.open ("https://drive.google.com/file/d/1NsfWjnvWxpX5S5k72voIWydlWXI5wlxu/view?usp=drivesdk"), '_blank';
}

function artigo(){
    window.open ("artigo.html", '_blank');
}

function guest_post(){
    window.open ("guest-post.html", '_blank');
}

function marketing(){
    window.open ("marketing-conteudo.html", '_blank');
}

function cliente(){
    window.open ("obter-mais-cliente.html", '_blank');
}

function rankeamento(){
    window.open ("rankeamento-google.html", '_blank');
}

function conte(){
    window.open ("conte-medtec-clientes.html", '_blank');
}

function seo(){
    window.open ("seo-on-page.html", '_blank');
}

(function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
    h._hjSettings={hjid:1656841,hjsv:6};
    a=o.getElementsByTagName('head')[0];
    r=o.createElement('script');r.async=1;
    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
    a.appendChild(r);
})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');